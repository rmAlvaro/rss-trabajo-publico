package com.cesur;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
/**
 * Hello world!
 *
 */
public class App 
{
    private static String serverDB="localhost";
    private static String portDB="1533";
    private static String DBname="RSS_Import";
    private static String userDB="sa";
    private static String passwordDB="12345Ab##";
   
    private static String url = "https://www.dulcespostres.com/feed/";
    //private static String url = "https://lenguajedemarcasybbdd.wordpress.com/feed/";

    /** 
     * @param args
     * @throws ParseException
     */
    public static void main( String[] args ) throws ParseException{

        boolean salida = false;

        do {
            int seleccion = mostrarMenu_devuelve_numero();
            switch(seleccion){
                case 0:
                CreacionBBDD.CrearBBDD();
                CreacionBBDD.CrearTablas();
                CreacionBBDD.insert_BBDD (url);
                break;

                case 1:
                consultas.ver_10post();
                System.out.println();
                break;

                case 2:
                consultas.ver_todosPost();
                System.out.println();
                break;

                case 3:
                consultas.buscar_por_id();
                System.out.println();
                break;

                case 4:
                consultas.buscar_titulo_contenido();
                System.out.println();
                break;

                case 5:
                consultas.filtrar_categoria();
                System.out.println();
                break;

                case 6:
                salida = true;
                break;

            }

        }while (salida == false);
  
    }

    
    // ----------------------------------------------------------------
    // ----------------------------------------------------------------
    private static String LeerScriptBBDD (String archivo){
       String ret = "";
        try {
            String cadena;
            FileReader f = new FileReader(archivo);
            BufferedReader b = new BufferedReader(f);
            while((cadena = b.readLine())!=null) {
                ret =ret +cadena;
            }
            b.close();
        } catch (Exception e) {
        }
       return ret;

    }
  
   
      // Creacion del menu 
      public static int mostrarMenu_devuelve_numero(){
        Scanner sc = new Scanner(System.in);
        int numero_Menu = -1;
        System.out.println("---------> Menu de RSS <-----------");
        System.out.println("0.- Crear BBDD.");
        System.out.println("1.- Ver ultimos 10 post.");
        System.out.println("2.- Ver todos los post.");
        System.out.println("3.- Ver detalles de un post por ID.");
        System.out.println("4.- Buscar por titulo o contenido una palabra.");
        System.out.println("5.- Filtrar por categoria, sin tildes");
        System.out.println("6.- Terminar programa. ");
        do {
            try {
                System.out.print("---------> Utiliza solo los numero para moverte por el menu: ");
                numero_Menu = sc.nextInt();
            }catch(Exception e){
                sc.next();
                System.out.println("Se ha prodocido un error al introducir un numero en el menu, indroduza un numero entre 1-4");
            }
        }while(numero_Menu<0 || numero_Menu>7);
       return numero_Menu;
    }

}



