package com.cesur;

import java.text.ParseException;
import java.util.*;

public class Post {

    public String autor = "Sin autor";
    public String contenido = "Sin contenido";
    public String fecha = "Sin fecha";
    public String categoria= "Sin categoria";
    public String link = "Sin link"; 
    public String titulo = "Sin titulo";

    public static ArrayList <Post> posts = new ArrayList<Post>();
    
    public Post (){
        
    }

    public void setTitulo (String titulo){
        this.titulo = titulo;
    }

    public void setAutor (String autor){
        this.autor = autor;
    }

    public void setcontenido (String contenido){
        this.contenido = contenido;
    }

    public void setfecha (String fecha){
        this.fecha = fecha;
    }

    public void setcategoria (String categoria){
        this.categoria = categoria;
    }

    public void setlink (String link){
        this.link = link;
    }

    public String getTitulo (){
       return this.titulo;
    }

    public String getAutor (){
        return this.autor;
     }

     public String getContenido(){
        return this.contenido;
     }

    public String getfecha (){
        return this.fecha;
    }

    public String getcategoria (){
        return this.categoria;
    }

    public String getlink (){
        return this.link;
    }

    private static void generarPosts_titulos (String url1){
        
        ArrayList <String> titulos =  Tabla_Contenido.insertTitulo(url1);
        
        for (String titulo:titulos){
            Post p = new Post ();
            p.setTitulo(titulo);
            posts.add(p);
        }
    }

    private static void generarPost_autor (String url1){
        ArrayList <String> autores = Tabla_Autor.insertAutor_BBDD(url1);
       for (int i = 0;i<autores.size();i++){
           posts.get(i).setAutor (autores.get(i));
       }
    }

    private static void generarPost_categoria(String url1){
        ArrayList <String> categoria =  Tabla_Categorias.inseterCategoria_BBDD(url1);

        for (int i = 0;i<categoria.size();i++){
            posts.get(i).setcategoria(categoria.get(i));
        }
    }

    private static void generarPost_contenido(String url1){
        ArrayList <String> contenido = Tabla_Contenido.insertDescripcion_BBDD(url1);
        for (int i = 0;i<contenido.size();i++){
            posts.get(i).setcontenido(contenido.get(i));
        }
    }

    private static void generarPost_link(String url1){
        ArrayList <String> links = Tabla_Post.insertLink_BBDD(url1);
        for (int i = 0;i<links.size();i++){
            posts.get(i).setlink(links.get(i));
        }
    }

    private static void generarPost_fechas (String url1) throws ParseException{
        ArrayList <String> fechas = Tabla_Post.insertFecha_BBDD(url1);
        for (int i = 0;i<fechas.size();i++){
            posts.get(i).setfecha(fechas.get(i));
        }
    }

    public void generarPost (String url1) throws ParseException{
        generarPosts_titulos(url1);
        generarPost_autor(url1);
        generarPost_categoria(url1);
        generarPost_contenido(url1);
        generarPost_link(url1);
        generarPost_fechas(url1);
    }


    public void mostrarPost (){
        for (int i = 0; i<posts.size();i++){
            System.out.println(posts.get(i).getTitulo()+" | "+posts.get(i).getAutor()+" | "+posts.get(i).getContenido());
            System.out.println(posts.get(i).getlink()+" | "+posts.get(i).getfecha()+" | "+posts.get(i).getcategoria());
            System.out.println("-------------------------------------------");
        }
    }

}


