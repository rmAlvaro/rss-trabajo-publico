package com.cesur;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;

public class Tabla_Post {
    public static ArrayList <String> insertFecha_BBDD(String url1) throws ParseException{
        XmlParse x =new XmlParse();
        String url = url1;
        ArrayList <String>  list = x.leer("//item/pubDate/text()",url);
        ArrayList<String> fechas = new ArrayList<>();
    
        list = x.leer("//item/pubDate/text()",url);
        
        for (String p:list){
    
            String str = p; 
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEE, dd MMM yyyy HH:mm:ss Z").withLocale(Locale.ENGLISH); 
            LocalDateTime dateTime = LocalDateTime.parse(str, formatter);
            String fecha = dateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss"));
    
            fechas.add(fecha);
        }
       
        System.out.println(("Numero de Fechas encontradas en el RSS: " + list.size()));
        
       return fechas;
        }

        public static ArrayList <String> insertLink_BBDD(String url1){
            XmlParse x =new XmlParse();
        String url = url1;
        ArrayList <String>  list = x.leer("//item/link/text()",url);
    
        list = x.leer("//item/link/text()",url);
        
        System.out.println(("Numero de link encontradas en el RSS: " + list.size()));
        return list;
        }
    
}
