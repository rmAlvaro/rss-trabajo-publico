package com.cesur;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class CreacionBBDD {

    private static String serverDB="localhost";
    private static String portDB="1533";
    private static String DBname="RSS_Import";
    private static String userDB="sa";
    private static String passwordDB="12345Ab##";
    private static int contador_duplicados = 0;

    public static void CrearBBDD(){
        bbdd b = new bbdd(serverDB,portDB,"master",userDB,passwordDB);
        
        String i= b.leerBBDDUnDato("SELECT count(1) FROM sys.databases  where name='"+DBname+"' ");
       // String cero = "0";
        if (i.equals("0")){
            b.modificarBBDD("CREATE DATABASE " + DBname);
            b=new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
        }
    }

    /*Hice tantos select por tabla porque en un principio queria crear las tablas por separado, en este modelo a lo mejor lo mas eficiente 
    seria preguntar por la primera tabla y si esta no existe crear todo. Lo dejo por si lo tengo que modificar y tengo que crear las tablas por separado (en el menu 
    ponga crear table x ...) */
    
    public static void CrearTablas(){
        bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);

        String categorias= b.leerBBDDUnDato("select COUNT (1) from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA = 'dbo' and TABLE_NAME = 'categorias'");
        String post= b.leerBBDDUnDato("select COUNT (1) from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA = 'dbo' and TABLE_NAME = 'post'");
        String contenido= b.leerBBDDUnDato("select COUNT (1) from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA = 'dbo' and TABLE_NAME = 'contenido'");
        String autor= b.leerBBDDUnDato("select COUNT (1) from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA = 'dbo' and TABLE_NAME = 'autor'");
        String trigger = b.leerBBDDUnDato("SELECT count (1) FROM sys.triggers WHERE name = 'asignar_categoria'");
        
        if (categorias.equals("0")){
            b.modificarBBDD("create table categorias (id_categoria int identity (1,1)  Primary Key,nombre_categoria varchar (100))");
        }

        if (contenido.equals("0")){
            b.modificarBBDD("create table contenido (id_contenido int identity (1,1)  Primary Key,titulo varchar (1200),descripcion varchar (max))");
        }

        if (autor.equals("0")){
            b.modificarBBDD("create table autor (id_autor int identity (1,1)  Primary Key, autor varchar (50))");
        }

        if (post.equals("0")){
            b.modificarBBDD("create table post (id_post int identity (1,1)  Primary Key,link varchar(200),fecha_subida DATETIME,id_categoria int,id_contenido int,id_autor int,constraint fk_contenido1 foreign key (id_contenido) references contenido (id_contenido),constraint fk_categorias1 foreign key (id_categoria) references categorias (id_categoria),constraint fk_autor1 foreign key (id_autor) references autor (id_autor),)");
        }

        //-------------- trigger
        if (trigger.equals("0")){
            b.modificarBBDD("insert into categorias values ('Sin Categoria')");
            b.modificarBBDD("insert into autor values ('Autor desconocido')");
    
            b.modificarBBDD("create trigger asignar_categoria on post for insert as begin Declare @c int = (Select id_categoria from inserted) Declare @id int = (Select id_post from inserted) Declare @a int = (Select id_autor from inserted) if (@c is null) begin set @c = (Select top (1) id_categoria from categorias where nombre_categoria = 'Sin Categoria') update post set id_categoria = @c where id_post = @id end if (@a is null) begin set @a = (Select top (1) id_autor from autor where autor = 'Autor desconocido') update post set id_autor = @a where id_post = @id end end");
    
            /*
            create trigger asignar_categoria 
            on post 
            for insert 
            as 
            begin
                    Declare @c int = (Select id_categoria from inserted)
                    Declare @id int = (Select id_post from inserted)
                    Declare @a int = (Select id_autor from inserted)
                if (@c is null)
                begin
                    set @c = (Select top (1) id_categoria from categorias where nombre_categoria = 'Sin Categoria')
                    update post set id_categoria = @c where id_post = @id
                end
                    
                if (@a is null)
                begin
                    set @a = (Select top (1) id_autor from autor where autor = 'Autor desconocido')
                    update post set id_autor = @a where id_post = @id
                end		
            end
    
    
            Prueba --> insert into post (link, fecha_subida, id_contenido) values ('inventado.com',GETDATE(),1) 
            */
        }
    }

    /////// ---- Insert -----------------

    public static void insert_BBDD (String url1) throws ParseException{
        Post p = new Post();
        p.generarPost(url1);

        ArrayList <String> categorias = new ArrayList<String>();

        for (int i = 0; i<p.posts.size();i++){
            categorias.add(p.posts.get(i).categoria);
        }
      
        insert_Categoria (categorias);

        for (int i = 0; i<p.posts.size();i++){
            insert_Autor (p.posts.get(i).autor);
            insert_Contenido (p.posts.get(i).contenido, p.posts.get(i).titulo);
            insert_Post (p.posts.get(i).autor, p.posts.get(i).categoria, p.posts.get(i).contenido,p.posts.get(i).link, p.posts.get(i).fecha);
        }
    }
    
    private static void insert_Autor (String autor){
        bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
        b.modificarBBDD("insert into autor (autor) values ('"+autor+"')");
    }

    private static void insert_Categoria ( ArrayList <String> categorias){
        bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);

        ArrayList <String> categorias_sin_repe = new ArrayList<String>(categorias);

        Set<String> hashSet = new HashSet<String>(categorias_sin_repe);
        categorias_sin_repe.clear();
        categorias_sin_repe.addAll(hashSet);

        for (int i = 0;i<categorias_sin_repe.size();i++){
            int id_contenido_repe = Integer.parseInt(b.leerBBDDUnDato("select count(*) from categorias where nombre_categoria = '"+categorias_sin_repe.get(i)+"'"));
            if (id_contenido_repe == 0){
            b.modificarBBDD("insert into categorias (nombre_categoria) values ('"+categorias_sin_repe.get(i)+"')");
            } 
        }

    }

    private static void insert_Contenido (String contenido, String titulo){
        bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);

        int id_contenido_repe = Integer.parseInt(b.leerBBDDUnDato("select count(*) from contenido where descripcion = '"+contenido+"'"));

        if (id_contenido_repe == 0){
        b.modificarBBDD("insert into contenido (titulo,descripcion) values ('"+titulo+"','"+contenido+"')");
        } else {
        contador_duplicados ++;
        System.out.println("No se ha insertado un registro al tener un contenido duplicado :( || Total:"+contador_duplicados+"");
        
    }

    }

    private static void insert_Post (String autor, String categoria, String contenido, String link, String fecha){
        bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
            int id_autor = Integer.parseInt(b.leerBBDDUnDato("select top(1) id_autor from autor where autor = '"+autor+"'"));
            int id_categoria = Integer.parseInt(b.leerBBDDUnDato("select top(1) id_categoria from categorias where nombre_categoria = '"+categoria+"'"));
            int id_contenido = Integer.parseInt(b.leerBBDDUnDato("select top(1) id_contenido from contenido where descripcion = '"+contenido+"'"));
            b.modificarBBDD("insert into post (link,fecha_subida,id_categoria,id_contenido,id_autor) values ('"+link+"','"+fecha+"','"+id_categoria+"','"+id_contenido+"','"+id_autor+"')");
    }

}
