package com.cesur;

import java.util.Scanner;

public class consultas {

    private static String serverDB="localhost";
    private static String portDB="1533";
    private static String DBname="RSS_Import";
    private static String userDB="sa";
    private static String passwordDB="12345Ab##";

    public static void ver_10post (){
        bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
        // Post incluye contenido
        b.leerBBDD("select top (10) co.titulo,co.descripcion,a.autor,p.fecha_subida,c.nombre_categoria from post p inner join categorias c on p.id_categoria = c.id_categoria inner join contenido co on p.id_contenido = co.id_contenido inner join autor a on p.id_autor = a.id_autor order by fecha_subida desc", 5); 
        
        // b.leerBBDD("select top (10) * from post order by fecha_subida desc", 6);
        
    }

    public static void ver_todosPost (){
        bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
        // Post incluye contenido
        b.leerBBDD("select co.titulo,co.descripcion,a.autor,p.fecha_subida,c.nombre_categoria from post p inner join categorias c on p.id_categoria = c.id_categoria inner join contenido co on p.id_contenido = co.id_contenido inner join autor a on p.id_autor = a.id_autor order by fecha_subida desc", 5); 
        
        // b.leerBBDD("select * from post order by fecha_subida desc", 6);
        
    } 


    public static void buscar_por_id (){
        Scanner sc = new Scanner (System.in);
        System.out.print("\nIntroduce el id de Post: ");
        String id = sc.nextLine();


        bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
        System.out.println();
        b.leerBBDD("select co.titulo,co.descripcion,a.autor,p.fecha_subida,c.nombre_categoria from post p inner join categorias c on p.id_categoria = c.id_categoria inner join contenido co on p.id_contenido = co.id_contenido inner join autor a on p.id_autor = a.id_autor  where p.id_post = "+id+"order by fecha_subida desc", 5); 

    }

    public static void buscar_titulo_contenido () {
        Scanner sc = new Scanner (System.in);
        System.out.print("\nIntroduce una palabra: ");
        String palabra = sc.nextLine();

        bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
        b.leerBBDD("select co.titulo,co.descripcion,a.autor,p.fecha_subida,c.nombre_categoria from post p inner join categorias c on p.id_categoria = c.id_categoria inner join contenido co on p.id_contenido = co.id_contenido inner join autor a on p.id_autor = a.id_autor  where co.descripcion like '%"+palabra+"%' or co.titulo like '%"+palabra+"%' order by fecha_subida desc", 5); 
    
        // Me salio este error con CONTAINS y lo he medio resuelto con like
        // Cannot use a CONTAINS or FREETEXT predicate on table or indexed view 'contenido' because it is not full-text indexed.
    
    }

    public static void filtrar_categoria () {
        bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);

        System.out.println("\nCategorias: ");
        b.leerBBDD("select nombre_categoria from categorias", 1);
        System.out.println("-------------------------------\n");

        Scanner sc = new Scanner (System.in);
        System.out.print("\nIntroduce una categoria: ");
        String categoria = sc.nextLine();

     
        b.leerBBDD("select co.titulo,co.descripcion,a.autor,p.fecha_subida,c.nombre_categoria from post p inner join categorias c on p.id_categoria = c.id_categoria inner join contenido co on p.id_contenido = co.id_contenido inner join autor a on p.id_autor = a.id_autor  where c.nombre_categoria = '"+categoria+"' order by fecha_subida desc", 5); 
    }

    
}
